package com.testapplication.luisroman.testapplication;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jakewharton.espresso.OkHttp3IdlingResource;
import com.testapplication.luisroman.testapplication.retrofit.RetrofitBuilder;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
public class SimpleTest {

    @Rule
    public IntentsTestRule<MainActivity> activityTestRule = new IntentsTestRule<MainActivity>(MainActivity.class,true,false);

    private IdlingResource idlingResource = OkHttp3IdlingResource.create("apptest", RetrofitBuilder.okHttpClient);

    private MockWebServer server = new MockWebServer();

    @Before
    public void setUp() throws IOException {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                InputStream jsonFile = null;
                byte[] jsonBytes = null;
                if (request.getPath().equals("/author")){
                    try {
                        jsonFile = InstrumentationRegistry.getContext().getAssets().open("author.json");
                        jsonBytes = IOUtils.toByteArray(jsonFile);
                    }
                    catch (IOException exception) {
                        exception.printStackTrace();
                    }
                    return new MockResponse().setResponseCode(200).setBody(new String(jsonBytes));
                } else if (request.getPath().equals("/author/Mocked%20Test/title")){
                    try {
                        jsonFile = InstrumentationRegistry.getContext().getAssets().open("title.json");
                        jsonBytes = IOUtils.toByteArray(jsonFile);
                    }
                    catch (IOException exception) {
                        exception.printStackTrace();
                    }
                    return new MockResponse().setResponseCode(200).setBody(new String(jsonBytes));
                } else if (request.getPath().equals("/author,title/Mocked%20Test;Mocked%20Test%20is%20the%20Way")) {
                    try {
                        jsonFile = InstrumentationRegistry.getContext().getAssets().open("poem.json");
                        jsonBytes = IOUtils.toByteArray(jsonFile);
                    }
                    catch (IOException exception) {
                        exception.printStackTrace();
                    }
                    return new MockResponse().setResponseCode(200).setBody(new String(jsonBytes));
                } else {
                    return new MockResponse().setResponseCode(404);
                }
            }
        };
        server.setDispatcher(dispatcher);
        server.start();
        String serverText = server.url("/").toString();
        RetrofitBuilder.mockedUrl = serverText;
        IdlingRegistry.getInstance().register(idlingResource);
        activityTestRule.launchActivity(null);
    }

    @After
    public void tearDown() throws IOException {
        activityTestRule.finishActivity();
        IdlingRegistry.getInstance().unregister(idlingResource);
        server.close();
        RetrofitBuilder.mockedUrl = null;
    }

    @Test
    public void testCase() {
        onData(anything()).inAdapterView(withId(R.id.mainListView)).atPosition(0).perform(click());
    }
}
