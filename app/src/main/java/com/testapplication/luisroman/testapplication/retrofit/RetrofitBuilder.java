package com.testapplication.luisroman.testapplication.retrofit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {

    public static OkHttpClient okHttpClient = new OkHttpClient();

    private static String baseUrl = "http://poetrydb.org/";

    public static String mockedUrl = null;

    public static Retrofit getRetrofitBuilder() {
        String url;
        if (mockedUrl != null) {
            url = mockedUrl;
        } else {
            url = baseUrl;
        }
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        return retrofit;
    }



}
