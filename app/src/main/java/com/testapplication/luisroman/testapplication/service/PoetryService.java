package com.testapplication.luisroman.testapplication.service;

import com.google.gson.JsonObject;
import com.testapplication.luisroman.testapplication.object.Poem;
import com.testapplication.luisroman.testapplication.object.Title;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PoetryService {

    @GET("author")
    Call<JsonObject> getAuthors();

    @GET("author/{name}/title")
    Call<List<Title>> getTitle(@Path("name") String name);

    @GET("author,title/{author};{title}")
    Call<List<Poem>> getPoem(@Path("author") String author, @Path("title") String title);

}
