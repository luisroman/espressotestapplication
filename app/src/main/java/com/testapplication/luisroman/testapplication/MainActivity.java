package com.testapplication.luisroman.testapplication;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.testapplication.luisroman.testapplication.retrofit.RetrofitBuilder;
import com.testapplication.luisroman.testapplication.service.PoetryService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mainListView)
    protected ListView view;

    private static JsonArray jsonElements = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        addAuthors();
    }

    private void addAuthors() {
        if (jsonElements == null) {
            PoetryService poetryService = RetrofitBuilder.getRetrofitBuilder().create(PoetryService.class);
            Call<JsonObject> authorsList = poetryService.getAuthors();
            authorsList.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    JsonObject listAuthors = response.body();
                    jsonElements = listAuthors.getAsJsonArray("authors");
                    setAuthorsToList(jsonElements);
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("debug", t.toString());
                }
            });
        } else {
            setAuthorsToList(jsonElements);
        }

    }

    private void setAuthorsToList(JsonArray jsonArray) {
        List<String> listStringAuthors = new ArrayList<>();
        for (int x = 0; x < jsonArray.size(); x++) {
            listStringAuthors.add(jsonArray.get(x).getAsString());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, listStringAuthors);
        view.setAdapter(adapter);
    }

    @OnItemClick(R.id.mainListView)
    protected void openPoem(AdapterView<?> parent, int position) {
        Intent intent = new Intent(MainActivity.this, PoemActivity.class);
        intent.putExtra("author", jsonElements.get(position).getAsString());
        startActivity(intent);
    }

}
