package com.testapplication.luisroman.testapplication.object;

import java.util.List;

public class Poem {

    private String title;
    private String author;
    private List<String> lines = null;
    private String linecount;

    public Poem(String title, String author, List<String> lines, String linecount) {
        this.title = title;
        this.author = author;
        this.lines = lines;
        this.linecount = linecount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<String> getLines() {
        return lines;
    }

    public void setLines(List<String> lines) {
        this.lines = lines;
    }

    public String getLinecount() {
        return linecount;
    }

    public void setLinecount(String linecount) {
        this.linecount = linecount;
    }
}
