package com.testapplication.luisroman.testapplication;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.testapplication.luisroman.testapplication.object.Poem;
import com.testapplication.luisroman.testapplication.object.Title;
import com.testapplication.luisroman.testapplication.retrofit.RetrofitBuilder;
import com.testapplication.luisroman.testapplication.service.PoetryService;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PoemActivity extends AppCompatActivity {

    @BindView(R.id.titlePoem)
    protected TextView titlePoem;

    @BindView(R.id.poemBody)
    protected ScrollView bodyPoem;

    @BindView(R.id.emailShare)
    protected ImageButton emailShare;

    private String author;
    private String title;
    private StringBuilder stringPoem = new StringBuilder();
    private PoetryService poetryService = RetrofitBuilder.getRetrofitBuilder().create(PoetryService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poem);
        ButterKnife.bind(this);
        emailShare.setEnabled(false);
        Bundle bundle = getIntent().getExtras();
        author = bundle.getString("author");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(author);
        setRandomPoemTitle();
    }

    private void setRandomPoemTitle() {
        Call<List<Title>> titleList = poetryService.getTitle(author);
        titleList.enqueue(new Callback<List<Title>>() {
            @Override
            public void onResponse(Call<List<Title>> call, Response<List<Title>> response) {
                Log.d("responsebodysize", Integer.toString(response.body().size()));
                int randomNumber = new Random().nextInt(response.body().size());
                title = response.body().get(randomNumber).getTitle();
                titlePoem.setText("Title: " + title);
                setPoemBody();
            }

            @Override
            public void onFailure(Call<List<Title>> call, Throwable t) {
                Log.d("debug", t.toString());
                title = "Error";
                titlePoem.setText(title);
            }
        });
    }

    private void setPoemBody() {
        Call<List<Poem>> poem = poetryService.getPoem(author, title);
        poem.enqueue(new Callback<List<Poem>>() {
            @Override
            public void onResponse(Call<List<Poem>> call, Response<List<Poem>> response) {
                List<String> poemLines = response.body().get(0).getLines();
                for (String text : poemLines) {
                    stringPoem.append(text + "\n");
                }
                TextView view = new TextView(PoemActivity.this);
                view.setId(R.id.poemTextDisplay);
                view.setText(stringPoem);
                bodyPoem.addView(view);
                emailShare.setEnabled(true);
            }

            @Override
            public void onFailure(Call<List<Poem>> call, Throwable t) {
                Log.d("debug", t.toString());

            }
        });
    }

    @OnClick(R.id.emailShare)
    protected void sharePoemByEmail(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, "Here you have your random poem");
        intent.putExtra(Intent.EXTRA_TEXT, "Author: " + author + "\n\n" + "Title: " + title + "\n\n" + stringPoem.toString());
        startActivity(intent);
    }
}
